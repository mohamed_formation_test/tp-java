package resultat;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;


//Resultat pour les exercices sur les tableaux � 2 dimensions.
public class Resultat6 extends Resultat {

	int[][] tabVoiture = new int[5][5];

	public void getSolution(int numero) {

		switch (numero) {
		case 40:
			solutionExo61();
			break;
		case 41:
			solutionExo62();
			break;
		case 42:
			solutionExo63();
			break;
		case 43:
			solutionExo64();
			break;
		case 44:
			solutionExo65();
			break;
		case 45:
			solutionExo66();
			break;
		case 46:
			solutionExo67();
			break;
		case 47:
			solutionExo68();
			break;
		case 48:
			solutionExo69();
			break;
		case 49:
			solutionExo70();
			break;
		case 50:
			solutionExo71();
			break;
		case 51:
			solutionExo72();
			break;

		}

	}

	private void solutionExo61() {
		int[][] tab = new int[10][4];
		int valeur = 0;
		int moyenne = 0;
		int produit = 1;
		int somme = 0;
		Random r = new Random();
		System.out.println("Merci de saisir une valeur entre 5 et 100 ");
		Scanner s = new Scanner(System.in);
		System.out.print("Reponse : ");
		valeur = s.nextInt();
		System.out.println(" ");
		for (int i = 0; i <= tab.length - 1; i++) {
			for (int j = 0; j <= tab[i].length - 1; j++) {
				tab[i][j] = r.nextInt(valeur) + 1;
				somme = somme + tab[i][j];
				produit = produit * tab[i][j];
			}
		}
		afficheTab(tab);
		System.out.println(" ");
		moyenne = somme / (10 * 4);
		System.out.println("La somme est de" + " " + somme + " " + ",le produit est de " + " " + produit + " "
				+ "et enfin, la moyenne est de" + " " + moyenne);

	}

	private void solutionExo62() {
		int dimension = 0;
		System.out.println("Merci de selectionner la dimension de votre matrice carré :");
		System.out.print("Reponse : ");
		Scanner s = new Scanner(System.in);
		dimension = s.nextInt();
		int sommeDiag = 0;
		int[][] tab1 = new int[dimension][dimension];
		for (int i = 0; i <= tab1.length - 1; i++) {
			for (int j = 0; j <= tab1[i].length - 1; j++) {
				tab1[i][j] = i + j;
				if (i == j) {
					sommeDiag = sommeDiag + tab1[i][j];
				}
			}
		}
		afficheTab(tab1);
		System.out.println("");
		System.out.println("La somme de la diagonale de la matrice de dimension" + " " + dimension + " " + "est de "
				+ " " + sommeDiag);
	}

	private void solutionExo63() {
		int dimension = 0;
		System.out.println("Merci de saisir la dimension de votre matrice :");
		System.out.print("Reponse : ");
		Scanner s = new Scanner(System.in);
		dimension = s.nextInt();
		int tab2[][] = new int[dimension][dimension];
		for (int i = 0; i < tab2.length; i++) {
			for (int j = 0; j <= i; j++) {
				if (j == 0 || i == j) {
					tab2[i][j] = 1;
				} else {
					tab2[i][j] = tab2[i - 1][j - 1] + tab2[i - 1][j];
				}
			}
		}
		afficheTab(tab2);
	}

	private void solutionExo64() {
		int[][] tab = new int[12][8];
		Random r = new Random();
		int max = 0;
		for (int i = 0; i < tab.length - 1; i++) {
			for (int j = 0; j < tab[i].length; j++) {
				tab[i][j] = r.nextInt(96);
			}
		}
		System.out.println("Le tableau :");
		System.out.println("");
		afficheTab(tab);
		for (int i = 0; i < tab.length; i++) {
			for (int j = 0; j < tab[i].length; j++) {
				if (i == 0 && j == 0) {
					max = tab[i][j];
				}
				if (tab[i][j] > max) {
					max = tab[i][j];
				}
			}
		}
		System.out.println("");
		System.out.println("Le nombre le plus grand du tableau à deux dimension(12,8) est" + " " + max);
	}

	private void solutionExo65() {

		for (int i = 0; i <= tabVoiture.length - 1; i++) {
			for (int j = 0; j <= tabVoiture[i].length - 1; j++) {

				if (i == 0 && j > 0) {
					System.out.println("Merci de saisir le numero du vendeur" + " " + j + 1 + ":");
					Scanner s = new Scanner(System.in);
					tabVoiture[i][j] = s.nextInt();
				}
				if (j == 0 && i > 0) {
					System.out.println("Merci de saisir le numero du modele" + " " + i + 1 + ":");
					Scanner s = new Scanner(System.in);
					tabVoiture[i][j] = s.nextInt();
				}
			}
		}
		System.out.println("Le tableau :");
		System.out.println("");
		afficheTab(tabVoiture);
		for (int i = 1; i <= tabVoiture.length - 1; i++) {
			for (int j = 1; j < tabVoiture[i].length; j++) {
				System.out.println("Merci de remplir les ventes pour le véhicule n°" + " " + tabVoiture[i][0] + " "
						+ "et le vendeur n°" + " " + tabVoiture[0][j]);
				Scanner s = new Scanner(System.in);
				tabVoiture[i][j] = s.nextInt();
			}
			System.out.println("");
			afficheTab(tabVoiture);
			System.out.println("");
		}
	}

	private void solutionExo66() {
		solutionExo65();
		int somme = 0;
		System.out.println("");
		for (int i = 1; i < tabVoiture.length; i++) {
			somme = 0;
			for (int j = 1; j < tabVoiture[i].length; j++) {
				somme = somme + tabVoiture[i][j];
			}
			System.out.println("La somme des ventes pour le véhicule n°" + " " + tabVoiture[i][0] + " " + "est de" + " "
					+ somme + " " + "véhicules.");
		}
	}

	private void solutionExo67() {
		solutionExo66();
		System.out.println("----");
		int[] prix = { 200, 300, 400, 500 };
		int sommeVenteVendeur = 0;
		for (int i = 1; i < tabVoiture.length; i++) {
			sommeVenteVendeur = 0;
			for (int j = 1; j < tabVoiture[i].length; j++) {
				sommeVenteVendeur = sommeVenteVendeur + (tabVoiture[j][i] * prix[j - 1]);
			}
			System.out.println("La somme des ventes pour le vendeur n°" + " " + tabVoiture[0][i] + " " + "est de" + " "
					+ sommeVenteVendeur + " " + "euros.");
		}

	}

	private void solutionExo68() {
		int[][] tab = new int[5][5];
		boolean antiSymetrique = true;
		for (int i = 0; i < tab.length; i++) {
			for (int j = 0; j < tab[i].length; j++) {
				System.out.println("Merci de saisir un nombre colonne" + " " + i + " " + "ligne" + " " + j + " " + ":");
				System.out.println("Reponse :");
				Scanner s = new Scanner(System.in);
				tab[i][j] = s.nextInt();
			}
		}
		for (int i = 0; i < tab.length; i++) {
			for (int j = 0; j < tab[i].length; j++) {
				if (tab[i][j] + tab[j][i] != tab[i][i] || tab[i][i] != 0) {
					antiSymetrique = false;
				}
			}
		}
		if (antiSymetrique == true) {
			System.out.println("Le tableau est antiSymetrique.");
		} else {
			System.out.println("Le tableau n'est pas antiSymetrique.");
		}
		System.out.println("");
		afficheTab(tab);

	}

	private void solutionExo69() {
		int[][] tab = { { 1, 2, 3 }, { 5, 9, 10, 89 }, { 1, 2 }, { 5, 6, 9, 8 } };
		int[][] tabb = { { 1, 2, 3, 66 }, { 5, 9, 1, 89 }, { 1, 2, 8, 21 }, { 5, 6, 9, 16 } };
		Boolean isRegulier = true;
		int select = 0;
		System.out.println("Tableau 1 :");
		afficheTab(tab);
		System.out.println(" ");
		System.out.println("Tableau 2 : ");
		afficheTab(tabb);
		System.out.println(" ");
		System.out.println("Merci de choisir un tableau à vérifier (1 ou 2) :");
		Scanner s = new Scanner(System.in);
		select = s.nextInt();
		if (select == 1) {
			for (int i = 0; i < tab[i].length - 2; i++) {
				if (tab[i].length != tab[i + 1].length) {
					isRegulier = false;
				}
			}
		} else {
			for (int i = 0; i < tabb[i].length - 2; i++) {
				if (tabb[i].length != tabb[i + 1].length) {
					isRegulier = false;
				}
			}
		}
		System.out.println("");
		if (isRegulier == true) {
			System.out.println("Le tableau est régulier.");
		} else {
			System.out.println("Le tableau n'est pas régulier.");
		}

	}

	private void solutionExo70() {
		boolean isRegulier1 = false;
		boolean isRegulier2 = false;
		boolean isDimension = false;
		boolean creationPossible = false;
		int longueur = 0;
		int[][] t1 = { { 1, 2, 3, 4 }, { 5, 6, 7, 8 }, { 9, 5, 7, 8 } };
		int[][] t2 = { { 5, 6, 8, 9 }, { 1, 5, 8, 7 }, { 1, 2, 8, 9 } };
		int[][] t3 = new int[t1.length][4];
		System.out.println("Tester le tableau t1?");
		System.out.println("Reponse : 1 - OK         2 - KO");
		Scanner s = new Scanner(System.in);
		if (s.nextInt() == 1) {
			isRegulier1 = isRegulierTab(t1);
		}
		System.out.println("Tester le tableau t2?");
		System.out.println("Reponse : 1 - OK         2 - KO");
		Scanner s2 = new Scanner(System.in);
		if (s2.nextInt() == 1) {
			isRegulier2 = isRegulierTab(t2);
		}

		if (t1.length == t2.length) {
			isDimension = true;
		}

		if (isDimension == true && isRegulier1 == true && isRegulier2 == true) {
			creationPossible = true;
			for (int i = 0; i < t3.length; i++) {
				for (int j = 0; j < t3[i].length; j++) {
					t3[i][j] = t1[i][j] + t2[i][j];
				}
			}
		}
		if (creationPossible == true) {
			System.out.println("");
			System.out.println("Les tableaux sont réguliers et de même dimension.");
			System.out.println("");
			System.out.println("Le tableau t1 :");
			System.out.println("");
			afficheTab(t1);
			System.out.println("");
			System.out.println("Le tableau t2 :");
			System.out.println("");
			afficheTab(t2);
			System.out.println("");
			System.out.println("La somme des 2 tableaux dans un autre tableau :");
			System.out.println("");
			afficheTab(t3);

		} else {
			System.out.println("Les tableaux ne sont pas réguliers ou pas de même dimension.");
		}

	}

	private void solutionExo71() {
		int[][] tab = new int[5][5];
		int sommeDiag = 0;
		for (int i = 0; i < tab.length; i++) {
			for (int j = 0; j < tab.length; j++) {
				System.out.println("Merci de saisir le nombre à l'emplacement -> ligne " + " " + i + " " + "colonne "
						+ " " + j + ":");
				System.out.println("Reponse : ");
				Scanner s = new Scanner(System.in);
				tab[i][j] = s.nextInt();
				if (i == j) {
					sommeDiag = sommeDiag + tab[i][j];
				}
			}
			System.out.println("");
			afficheTab(tab);
		}
		System.out.println("");
		System.out.println("La somme de la diagonale du tableau est de :" + " " + sommeDiag + ".");

	}

	private void solutionExo72() {
		int[][] tab = { { 3, 16, 9, 22, 15 }, { 20, 8, 21, 14, 2 }, { 7, 25, 13, 1, 19 }, { 24, 12, 5, 18, 6 },
				{ 11, 4, 17, 10, 23 } };
		boolean magique = true;
		int sommeTest = 0;
		int sommeC = 0;
		int sommeL = 0;
		int sommeD = 0;
		System.out.println("Le tableau testé :");
		System.out.println("");
		afficheTab(tab);
		System.out.println("");
		for (int i = 0; i < tab[0].length; i++) {
			sommeTest = sommeTest + tab[0][i];
		}
		for (int i = 0; i < tab.length; i++) {
			sommeC = 0;
			sommeL = 0;
			for (int j = 0; j < tab[i].length; j++) {
				if (i == j) {
					sommeD = sommeD + tab[i][j];
				}
				sommeC = sommeC + tab[j][i];
				sommeL = sommeL + tab[i][j];
			}
			if (sommeC != sommeTest || sommeL != sommeTest) {
				magique = false;
			}
		}
		if (sommeD != sommeTest) {
			magique = false;
		}
		if (magique == true) {
			System.out.println("La carré est magique.");
		} else {
			System.out.println("Le carré n'est pas magique.");
		}
	}

	// Methodes hors exo :
	private void afficheTab(int[][] tableau) {
		for (int i = 0; i < tableau.length; i++) {
			for (int j = 0; j < tableau[i].length; j++) {
				System.out.print("[" + tableau[i][j] + "]");
			}
			System.out.println("");
		}
	}

	private void afficheTabChar(char[] tableau) {
		System.out.println(Arrays.toString(tableau));
	}

	private boolean isRegulierTab(int[][] tab) {
		boolean isRegulier = true;
		for (int i = 0; i < tab[i].length - 2; i++) {
			if (tab[i].length != tab[i + 1].length) {
				isRegulier = false;
			}
		}
		return isRegulier;

	}

	private void verificationCroissantTab(int[] tableau) {
		boolean trie = true;
		for (int i = 0; i <= tableau.length - 2; i++) {
			if (tableau[i] > tableau[i + 1]) {
				trie = false;
				i = tableau.length - 1;
			}
		}
		if (trie == true) {
			System.out.println("Le tableau trié de maniere croissante.");
		} else {
			System.out.println("Le tableau n'est pas trié de manière croissante.");
		}

	}
}
