package resultat;

import java.util.Arrays;
import java.util.Scanner;


//Resultat pour les exercices sur les tableaux
public class Resultat5 extends Resultat {

	private int[] tab = { 12, 1, 45, 89, 16, 35, 98, 12 };
	private int[] tab2 = { 1, 3, 4, 9, 12, 45, 89, 100 };
	private char[] tabDecalage = { 'D', 'E', 'C', 'A', 'L', 'A', 'G', 'E' };

	public void getSolution(int numero) {

		switch (numero) {
		case 33:
			solutionExo51Tableau();
			break;
		case 34:
			solutionExo52Tableau();
			break;
		case 35:
			solutionExo53Tableau();
			break;
		case 36:
			solutionExo54Tableau();
			break;
		case 37:
			solutionExo55Tableau();
			break;
		case 38:
			solutionExo56Tableau();
			break;
		case 39:
			solutionExo57Tableau();
			break;

		}

	}

	private void solutionExo51Tableau() {
		System.out.println("Le tableau :");
		afficheTab(tab);
		int nombre = 0;
		int occurence = 0;
		System.out.println("");
		System.out.println("Merci de saisir le nombre à vérifier?");
		Scanner s = new Scanner(System.in);
		nombre = s.nextInt();
		for (int i = 0; i <= tab.length - 1; i++) {
			if (nombre == tab[i]) {
				occurence++;
			}
		}
		System.out.println("");
		System.out.println("Le nombre" + " " + nombre + " " + "est présent" + " " + occurence + " " + "fois.");
	}

	private void solutionExo52Tableau() {
		int nombre = 0;
		System.out.println("Les tableaux :");
		System.out.println("");
		System.out.println("Le tableau 1 :");
		afficheTab(tab);
		System.out.println("");
		System.out.println("Le tableau 2 :");
		afficheTab(tab2);
		System.out.println("");
		System.out.println("Selctionnez le tableau à tester (1 ou 2) :");
		System.out.println("");
		System.out.print("Reponse : ");
		Scanner s1 = new Scanner(System.in);
		nombre = s1.nextInt();
		if (nombre == 1) {
			verificationCroissantTab(tab);
		} else if (nombre == 2) {
			verificationCroissantTab(tab2);
		}
	}

	private void solutionExo53Tableau() {
		System.out.println("Le tableau :");
		afficheTab(tab);
		int nombre = 0;
		int max = 0;
		int min = 0;
		int ecart = 0;
		for (int i = 0; i <= tab.length - 1; i++) {
			if (i == 0) {
				min = tab[0];
				max = tab[0];
			} else if (tab[i] > max) {
				max = tab[i];

			} else if (tab[i] < min) {

				min = tab[i];
			}
		}

		ecart = max - min;
		System.out.println("");
		System.out.println("Le plus grand ecart entre deux nombre dans ce tableau est de" + " " + ecart + ".");
	}

	private void solutionExo54Tableau() {
		System.out.println("Le tableau :");
		afficheTabChar(tabDecalage);
		char temp = ' ';
		for (int i = 0; i < tabDecalage.length - 1; i++) {
			temp = tabDecalage[i];
			tabDecalage[i] = tabDecalage[i + 1];
			tabDecalage[i + 1] = temp;
		}
		System.out.println("Le tableau decalé :");
		afficheTabChar(tabDecalage);
	}

	private void solutionExo55Tableau() {
		System.out.println("Le tableau :");
		afficheTab(tab2);
		int i = 0;
		int j = tab2.length - 1;
		int temp = 0;
		while (i < j) {
			temp = tab2[i];
			tab2[i] = tab2[j];
			tab2[j] = temp;
			i++;
			j--;
		}
		System.out.println("");
		System.out.println("Le tableau inversé:");
		afficheTab(tab2);
	}

	private void solutionExo56Tableau() {
		System.out.println("Le tableau :");
		afficheTab(tab);
		System.out.println("");
		System.out.println("Les etapes:");
		int min = 0;
		int indiceMin = 0;
		int indice = 0;
		int temp = 0;

		while (indice < tab.length) {

			min = tab[indice];

			for (int i = indice; i < tab.length; i++) {

				if (tab[i] < min) {
					min = tab[i];
					indiceMin = i;

				}
			}
			temp = tab[indice];
			tab[indice] = tab[indiceMin];
			tab[indiceMin] = temp;
			afficheTab(tab);
			indice++;

		}
		System.out.println("");
		System.out.println("Le tableau trié croissant (Selection):");
		afficheTab(tab);
	}

	private void solutionExo57Tableau() {
		System.out.println("Le tableau :");
		afficheTab(tab);
		int temp = 0;
		int i = 0;
		while (i <= tab.length-1) {
			for (int j = 0; j < (tab.length-1)-i; j++) {
				if (tab[j] > tab[j + 1]) {
					temp = tab[j];
					tab[j] = tab[j + 1];
					tab[j + 1] = temp;
				}
			}
			i++;
		}
		System.out.println("Le tableau avec tri à bulle :");
		afficheTab(tab);
	}

	// Methodes hors exo :
	private void afficheTab(int[] tableau) {
		System.out.println(Arrays.toString(tableau));
	}

	private void afficheTabChar(char[] tableau) {
		System.out.println(Arrays.toString(tableau));
	}

	private void verificationCroissantTab(int[] tableau) {
		boolean trie = true;
		for (int i = 0; i <= tableau.length - 2; i++) {
			if (tableau[i] > tableau[i + 1]) {
				trie = false;
				i = tableau.length - 1;
			}
		}
		if (trie == true) {
			System.out.println("Le tableau trié de maniere croissante.");
		} else {
			System.out.println("Le tableau n'est pas trié de manière croissante.");
		}

	}
}
