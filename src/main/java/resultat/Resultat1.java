package resultat;

// Resultat pour les exercices sur les variables
public class Resultat1 extends Resultat {


    public void getSolution(int numero) {

        switch (numero) {
            case 1:
                solutionExo11();
                break;
            case 2:
                solutionExo12();
                break;
            case 3:
                solutionExo13();
                break;
            case 4:
                solutionExo14();
                break;
            case 5:
                solutionExo15();
                break;
            case 6:
                solutionExo16();
                break;
            case 7:
                solutionExo17();
                break;
            case 8:
                solutionExo18();
                break;
            case 9:
                solutionExo19();
                break;
            default:
                getSolution(numero);
        }

    }

    public  void solutionExo11() {
        int A, B = 0;
        A = 1;
        B = A + 3;
        A = 3;
        System.out.print("La valeur de A est de" + " " + A + " " + " et celle de B est de" + " " + B);
    }

    public void solutionExo12() {
    int A, B, C = 0;
        A = 5;
        B = 3;
        C = A + B;
        A = 2;
        C = B - A;
        System.out.print("La valeur de A est de" + " " + A + " " + ", celle de B est de " + " " + B + " " + "et celle de C est de" + " " + C);
    }

    public void solutionExo13() {
        int A, B = 0;
        A = 5;
        B = A + 4;
        A = A + 1;
        B = A - B;

        System.out.print("La valeur de A est de" + " " + A + " " + "et celle de B est de " + " " + B + ".");
    }

    public void solutionExo14() {
        int A, B, C = 0;
        A = 3;
        B = 10;
        C = A + B;
        A = C;

        System.out.print("La valeur de A est de" + " " + A + " " + ", celle de B est de " + " " + B + " " + "et celle de C est de" + " " + C);
    }

    public void solutionExo15() {
        int A, B = 0;
        A = 5;
        B = 2;
        A = B;
        B = A;

        System.out.print("La valeur de A est de" + " " + A + " " + " et celle de B est de " + " " + B + ".");
    }

    public void solutionExo16() {
        int A, B, C = 0;
        A = 5;
        B = 8;
        C = A;
        A = B;
        B = C;

        System.out.print("int A, B, C = 0 \n Debut \n A = 5 \n B = 8 \n C = A \n A = B \n B = C \n Fin \n A =" + " " + A + "\n B =" + " " + B);
    }

    public void solutionExo17() {
        int A, B, C, D = 0;
        A = 5;
        B = 8;
        C = 3;
        D = C;
        C = B;
        B = A;
        A = D;

        System.out.print("int A, B, C, D = 0 \n Debut \n A = 5 \n B = 8 \n C = 3 \n D = C \n C = B \n B = A \n A = D \n Fin \n A =" + " " + A + "\n B =" + " " + B +"\n C =" +" "+ C);
    }
    public void solutionExo18() {
        String A, B, C;
        A = "423";
        B = "12";
        C = A + B;

        System.out.print("Le resultat est" + " " + C );
    }
    public void solutionExo19() {
        String A, B, C;
        A = "423";
        B = "12";
        C = A + B;

        System.out.print("Le resultat est" + " " + C );
    }
}
