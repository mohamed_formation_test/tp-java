package resultat;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

//Resultat pour les exercices sur les fonctions et les proc�dures.
public class Resultat7 extends Resultat {

	int[][] tabVoiture = new int[5][5];

	public void getSolution(int numero) {

		switch (numero) {
		case 52:
			solutionExo71();
			break;
		case 53:
			solutionExo72();
			break;
		case 54:
			solutionExo73();
			break;
		case 55:
			solutionExo74();
			break;
		case 56:
			solutionExo75();
			break;
		case 57:
			solutionExo76();
			break;
		case 58:
			solutionExo77();
			break;
		case 59:
			solutionExo78();
			break;
		case 60:
			solutionExo79();
			break;

		}

	}

	private void solutionExo71() {
//		String resultat = " ";
//		String trad = "";
//		String info = " ";
//		Scanner s = new Scanner(System.in);
//		info = s.nextLine();
//		resultat = strToBinary(info);
//		System.out.println(resultat);
//		int i = 0;
//		while (i < resultat.length() - 1) {
//
//			if (i == 0) {
//				if (resultat.charAt(i) == '1' && resultat.charAt(i + 1) == '0') {
//					trad = trad + "0" + " " + "0";
//				}
//			} else {
//				if (resultat.charAt(i) == '0' && resultat.charAt(i + 1) == '0' && resultat.charAt(i - 1) == '1') {
//					trad = trad + " " + "00" + " ";
//				}
//				if (resultat.charAt(i) == '0' && resultat.charAt(i + 1) == '0' && resultat.charAt(i - 1) == '0') {
//					trad = trad + "00";
//				}
//				if (resultat.charAt(i) == '0' && resultat.charAt(i + 1) == '1') {
//					trad = trad + " ";
//				}
//				if (resultat.charAt(i) == '1' && resultat.charAt(i + 1) == '1' && resultat.charAt(i - 1) == '0') {
//					trad = trad + "0" + " " + "0";
//				}
//				if (resultat.charAt(i) == '1' && resultat.charAt(i + 1) == '1') {
//					trad = trad + "0";
//				}
//				if (resultat.charAt(i) == '1' && resultat.charAt(i + 1) == ' ') {
//					trad = trad + " ";
//				}
//				if (resultat.charAt(i) == '1' && resultat.charAt(i + 1) == '0') {
//					trad = trad + " ";
//				}
//			}
//			i++;
//
//		}

//		System.out.print(trad);

		
		int[] suite = new int[7];
		
		suite[0] = 1;
		suite[1] = 1;
		for(int j = 2; j<=suite.length-1; j++) {
			
			suite[j] = suite[j-1] + suite[j-2];
		}
		
		System.out.println(Arrays.toString(suite));
		
	}

	private void solutionExo72() {

	}

	private void solutionExo73() {

	}

	private void solutionExo74() {
		int nbrePot = 0;
		int longueur = 0;
		int largeur = 0;
		System.out.println("Merci de saisir la longueur de la surface :");
		System.out.println("Longueur : ");
		Scanner s = new Scanner(System.in);
		longueur = s.nextInt();
		System.out.println("Merci de saisir la largeur de la surface :");
		System.out.println("Largeur : ");
		largeur = s.nextInt();
		nbrePot = fonctionSurface(largeur, longueur);
		System.out.println("");
		System.out.println("Le nombre de pots de shampoing pour la surface est de" + " " + nbrePot + ".");

	}

	private void solutionExo75() {
		int heure = 0;
		int minute = 0;
		int seconde = 0;
		int res = 0;
		System.out.println("Merci de me donner le nombre pour l'heure, puis les minutes et enfin les secondes :");
		System.out.println("Heure :");
		Scanner s = new Scanner(System.in);
		heure = s.nextInt();
		System.out.println("Minute :");
		Scanner s1 = new Scanner(System.in);
		minute = s1.nextInt();
		System.out.println("Seconde :");
		Scanner s2 = new Scanner(System.in);
		seconde = s2.nextInt();
		System.out.println("");
		res = conversionSecondes(heure, minute, seconde);
		System.out.println("Le resultat de la conversion en seconde est de" + " " + res + ".");

	}

	private void solutionExo76() {
		int[] tab = { 12, 1, 45, 89, 16, 35, 98, 12 };
		int taille = tab.length;
		int pointeur = tab.length - 1;
		int resultat = 0;
		resultat = somTab(tab, taille, pointeur);
		System.out.println("La somme du tableau est de :" + " " + resultat);
	}

	private void solutionExo77() {
		int res = 0;
		int nombre = 0;
		System.out.println("Saisir le nombre : ");
		Scanner s = new Scanner(System.in);
		nombre = s.nextInt();
		res = recursive(nombre);
		System.out.println("Resultat pour le nombre" + " " + nombre + " " + ":");
		System.out.print(res);
	}

	private void solutionExo78() {
		int s = 0;
		int t = 0;
		int res = 0;
		System.out.println("Merci de saisir les 2 nombres de la multiplication : ");
		System.out.println("Nombre 1");
		Scanner su = new Scanner(System.in);
		s = su.nextInt();
		System.out.println("");
		System.out.println("Nombre 2");
		Scanner so = new Scanner(System.in);
		t = so.nextInt();
		res = recursiveSansOperateur(s, t);
		System.out.println(res);
	}

	private void solutionExo79() {
		int[] t = { 1, 1, 3, 5, 9, 10, 89, 1, 2, 5, 6, 9, 8 };
		afficheTab(t);
		System.out.println("");
		int val = 0;
		int compteur = 0;
		int nombreDemarrageTableau = 0;
		int occurence = 0;
		System.out.println("Saisir un nombre pour vérifier son nombre d'occurence dans le tableau :");
		System.out.print("Reponse :");
		Scanner s = new Scanner(System.in);
		val = s.nextInt();
		occurence += recursiveOccurence(t, val, nombreDemarrageTableau);
		System.out.println("");
		System.out.println("Le nombre d'occurence dans le tableau T pour le nombre" + " " + val + " " + "est de" + " "
				+ occurence);
	}

	// Methodes hors exo :
	private void afficheTab(int[] tableau) {
		for (int i = 0; i < tableau.length; i++) {
			System.out.print("[" + tableau[i] + "]");
		}
		System.out.println("");
	}

	private void afficheTabChar(char[] tableau) {
		System.out.println(Arrays.toString(tableau));
	}

	private boolean isRegulierTab(int[][] tab) {
		boolean isRegulier = true;
		for (int i = 0; i < tab[i].length - 2; i++) {
			if (tab[i].length != tab[i + 1].length) {
				isRegulier = false;
			}
		}
		return isRegulier;

	}

	private void verificationCroissantTab(int[] tableau) {
		boolean trie = true;
		for (int i = 0; i <= tableau.length - 2; i++) {
			if (tableau[i] > tableau[i + 1]) {
				trie = false;
				i = tableau.length - 1;
			}
		}
		if (trie == true) {
			System.out.println("Le tableau trié de maniere croissante.");
		} else {
			System.out.println("Le tableau n'est pas trié de manière croissante.");
		}
	}

	private int recursive(int n) {
		int s = 0;
		if (n == 0) {
			s = 0;
		} else {
			s = n * n + recursive(n - 1);
		}
		return s;
	}

	private int recursiveSansOperateur(int n, int t) {
		if (t <= 0) {
			return 0;
		} else {
			return n + recursiveSansOperateur(n, t - 1);
		}
	}

	private int recursiveOccurence(int[] tableau, int a, int b) {
		int compteur = 0;
		if (b < tableau.length - 1) {
			if (tableau[b] == a) {
				compteur = 1;
			}
			return compteur + recursiveOccurence(tableau, a, b + 1);
		}
		return 0;
	}

	private int fonctionSurface(int largeur, int longueur) {
		int surface = largeur * longueur;
		int pot = 10;

		return surface / pot;
	}

	private int conversionSecondes(int heure, int minute, int seconde) {

		return heure * 3600 + minute * 60 + seconde;

	}

	private void maProcedure(int a, int b, int c, int d) {

		c = a + b;
		d = a * b;

		System.out.println(c);
		System.out.println(d);
	}

	private int somTab(int[] tab, int longueur, int pointeur) {
		longueur = tab.length;
		if (pointeur >= 0) {
			return tab[pointeur] + somTab(tab, longueur, pointeur - 1);
		} else {
			return 0;
		}
	}

	static String strToBinary(String s) {
		int n = s.length();
		String bin = "";
		for (int i = 0; i < n; i++) {
			// convert each char to
			// ASCII value
			int val = Integer.valueOf(s.charAt(i));

			// Convert ASCII value to binary
			while (val > 0) {
				if (val % 2 == 1) {
					bin += '1';
				} else
					bin += '0';
				val /= 2;
			}
			bin = reverse(bin);

		}
		return bin;
	}

	static String reverse(String input) {
		char[] a = input.toCharArray();
		int l, r = 0;
		r = a.length - 1;

		for (l = 0; l < r; l++, r--) {
			// Swap values of l and r
			char temp = a[l];
			a[l] = a[r];
			a[r] = temp;
		}
		return String.valueOf(a);
	}
}