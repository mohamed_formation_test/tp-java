package resultat;

import java.util.Scanner;



//Resultat pour les exercices sur les affecations sur les variables.
public class Resultat2 extends Resultat{

    public  void getSolution(int numero) {

        switch (numero) {
            case 10:
                solutionExo21();
                break;
            case 11:
                solutionExo22();
                break;
            case 12:
                solutionExo23();
                break;
            case 13:
                solutionExo24();
                break;

        }

    }

    public  void solutionExo21() {
        int val, doublee = 0;
        val = 231;
        doublee = val * 2;
        System.out.print("La valeur de val est de" + " " + val + " " + "et celle de doublee est de" + " " + doublee + ".");
    }


    public  void solutionExo22() {
        int nombre = 0;
        int carre = 0;
       System.out.println("Merci de saisir le nombre à calculer :");
        Scanner s = new Scanner(System.in);
       nombre = s.nextInt();
       carre = nombre * nombre;
       System.out.print("Le carre du nombre" + " " + nombre + " " + "est" + " " + carre + ".");
    }

    public  void solutionExo23() {
       String nom = " ";
        System.out.println("Machine : Quel est votre prénom?");
        Scanner s = new Scanner(System.in);
        nom = s.next();
        System.out.print("Utilisateur :" + " " + nom + " " + "\nMachine : Bonjour," + " " + nom + "!");
    }

    public  void solutionExo24() {
        int prix = 0;
        int nombre = 0;
        float taux = 0;
        double total = 0;
        Scanner s = new Scanner(System.in);
        System.out.println("Quel est le prix de l'article HT?");
        prix = s.nextInt();
        System.out.println("Quel est le taux de TVA (exemple 3 pour 3%) ?");
        taux = s.nextFloat();
        System.out.println("Quel est le nombre d'article?");
        nombre = s.nextInt();
        total = (prix * (1 + (taux/100))) * nombre;
        System.out.print("La prix total pour le produit au prix de" + " " + prix + " " + "euros HT, avec un taux de TVA de" + " " + taux +"% et une quantité de" + " " +  nombre + " " + "est de :" + " " + total +" "+"euros.");
    }


}
