package resultat;

import java.util.Scanner;

//Resultat pour les exercices sur les op�rateurs logiques
public class Resultat3 extends  Resultat{

    public void getSolution(int numero) {

        switch (numero) {
            case 14:
                solutionExo31();
                break;
            case 15:
                solutionExo32();
                break;
            case 16:
                solutionExo33();
                break;
            case 17:
                solutionExo34();
                break;
            case 18:
                solutionExo35();
                break;
            case 19:
                solutionExo36();
                break;
            case 20:
                solutionExo37();
                break;


        }

    }

    public  void solutionExo31() {
        int nombre = 0;
        System.out.println("Merci de saisir le nombre :");
        Scanner s = new Scanner(System.in);
        nombre = s.nextInt();
        if (nombre < 0) {
            System.out.print("Le nombre" + " " + nombre + " " + "est négatif.");
        } else {
            System.out.print("Le nombre" + " " + nombre + " " + "est positif.");
        }
    }


    public  void solutionExo32() {
        int nombre1 = 0;
        int nombre2 = 0;
        System.out.println("Merci de saisir le premier nombre :");
        Scanner s = new Scanner(System.in);
        nombre1 = s.nextInt();
        System.out.println("Merci de saisir le second nombre :");
        Scanner s1 = new Scanner(System.in);
        nombre2 = s1.nextInt();

        if (nombre1 < 0 && nombre2 < 0 || nombre1 > 0 && nombre2 > 0) {
            System.out.print("Le produit de" + " " + nombre1 + " " + "et de" + " " + nombre2 + " " + "est positif ");
        } else {
            System.out.print("Le produit de" + " " + nombre1 + " " + "et de " + " " + nombre2 + " " + "est negatif ");

        }

    }

    public  void solutionExo33() {
        String nom = " ";
        String nom2 = " ";
        String nom3 = " ";
        System.out.println("Quel est le nom 1 ?");
        Scanner s = new Scanner(System.in);
        nom = s.next();
        System.out.println("Quel est le nom 2 ?");
        Scanner s2 = new Scanner(System.in);
        nom2 = s2.next();
        System.out.println("Quel est le nom 3 ?");
        Scanner s3 = new Scanner(System.in);
        nom3 = s3.next();
        if (nom.compareTo(nom2) < 0 && nom2.compareTo(nom3) < 0) {
            System.out.print("Les noms" + " " + nom + ", " + nom2 + ", " + nom3 + " " + "sont rangés dans l'ordre alphabétique.");
        } else {
            System.out.print("Les noms" + " " + nom + ", " + nom2 + ", " + nom3 + " " + "ne sont pas rangés dans l'ordre alphabétique.");
        }
    }

    public  void solutionExo34() {
        int nombre = 0;
        System.out.println("Merci de saisir un nombre?");
        Scanner s3 = new Scanner(System.in);
        nombre = s3.nextInt();

        if (nombre == 0) {
            System.out.println("Le nombre est null");
        } else if (nombre < 0) {
            System.out.println("Le nombre est négatif");
        } else {
            System.out.println("Le nombre est positif");
        }
    }

    public  void solutionExo35() {
        int age = 0;
        System.out.println("Merci de saisir l'age de l'enfant?");
        Scanner s3 = new Scanner(System.in);
        age = s3.nextInt();
        if (age >= 12) {
            System.out.println("Il est dans la catégorie Cadet");
        } else if (age < 12 && age >= 10) {
            System.out.println("Il est dans la catégorie Minime");
        } else if (age < 10 && age >= 8) {
            System.out.println("Il est dans la catégorie Pupille");
        } else if (age < 8 && age >= 6) {
            System.out.println("Il est dans la catégorie Poussin");
        }
    }

    public  void solutionExo36() {
        int nombre = 0;
        System.out.println("Merci de saisir un nombre entier?");
        Scanner s3 = new Scanner(System.in);
        nombre = s3.nextInt();

        if (nombre%3 == 0) {
            System.out.println("Le nombre est divisible par 3.");
        } else{
            System.out.println("Le nombre n'est pas divisible par 3.");
        }
    }

    public  void solutionExo37() {
        int nombre = 0;
        double total = 0;
        System.out.println("Merci de saisir le nombre de photocopies à réaliser ?");
        Scanner s = new Scanner(System.in);
        nombre = s.nextInt();
        if (nombre > 20) {
    total = nombre * 0.05;
        } else if(nombre <= 20 && nombre >10){
            total = nombre * 0.10;
        }else{
            total = nombre * 0.15;
        }
        System.out.println("Le prix à payer est de" +" "+ total +" "+ "euros");
    }

}
