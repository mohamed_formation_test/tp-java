package main;

import java.io.IOException;
import java.util.InputMismatchException;
import java.util.Scanner;

import resultat.Resultat;
import resultat.Resultat1;
import resultat.Resultat2;
import resultat.Resultat3;
import resultat.Resultat4;
import resultat.Resultat5;
import resultat.Resultat6;
import resultat.Resultat7;
import solution.Solution5;

public class Menu {

// Menu principal lancer dans la Class Main. Elle lance le programme. Elle affiche la partie principale du menu
	public static void lancement(int selection, int selection2, int selection3) throws InputMismatchException {

		try {
			while (selection != 0) {
				System.out.println("********************");
				System.out.println("- Seletionner la th�matique d'exercice en algorithmie :");
				System.out.println(" ");
				System.out.println("1 - Exercices sur les variables.");
				System.out.println("2 - Exercices sur les operations sur les variables.");
				System.out.println("3 - Exercices sur les operateurs logiques.");
				System.out.println("4 - Exercices sur les boucles.");
				System.out.println("5 - Exercices sur les tableaux.");
				System.out.println("6 - Exercices sur les tableaux à 2 dimensions.");
				System.out.println("7 - Exercices sur les fonctions.");
				System.out.println("8 - Autres.");
				System.out.println("0 - Quitter");
				System.out.println(" ");
				System.out.print("Reponse : ");
				Scanner s = new Scanner(System.in);
				selection = s.nextInt();
				if (selection != 0) {
					sousMenu(selection, selection2, selection3);
				}
			}
		} catch (InputMismatchException e) {
			System.out.println(" ");
			System.out.println("Erreur de saisie : Merci de saisir un num�ro indiqu� dans le menu principal");
			System.out.println(" ");
			lancement(30, 30, 30);
		}
	}

// La methode sousMenu lanc� dans la methode lancement. Elle lance la seconde partie du menu en lien avec la selection dans le menu principal. 
//Elle permet de choisir l'exercie en fonction du th�me dans la methode lancement
	public static void sousMenu(int nombre, int selection2, int selection3) throws InputMismatchException {
		try {
			while (selection2 != 0 && nombre != 0) {
				switch (nombre) {
				case 1:
					System.out.println(" ");
					System.out.println("- Exercices sur les variables -");
					System.out.println(" ");
					System.out.println("Selectionner un exercice :");
					System.out.println(" ");
					System.out.println("1 - Exercice 1.1");
					System.out.println("2 - Exercice 1.2");
					System.out.println("3 - Exercice 1.3");
					System.out.println("4 - Exercice 1.4");
					System.out.println("5 - Exercice 1.5");
					System.out.println("6 - Exercice 1.6");
					System.out.println("7 - Exercice 1.7");
					System.out.println("8 - Exercice 1.8");
					System.out.println("9 - Exercice 1.9");
					System.out.println("0 - Retour");
					System.out.println(" ");
					System.out.print("Reponse : ");
					Scanner s2 = new Scanner(System.in);
					selection2 = s2.nextInt();
					if (selection2 != 0) {

						if (Enonce.get(selection2) == null) {
							System.out.println("Erreur de saisie : Le nombre n'existe pas");
							sousMenu(nombre, selection2, selection3);
						} else {
							System.out.println("");
							System.out.println(Enonce.get(selection2));
							Resultat res = new Resultat1(); // utilisation de la Class Abstract Resultat afin de pouvoir utiliser la methode sousMenu2() avec plusieurs Class Resultat1, Resultat2, etc.
							Solution sol5 = new Solution5(); // utilisation de la Class Abstract Solution afin de pouvoir utiliser la methode sousMenu2() avec plusieurs Class Solution1, Solution2, etc.
							sousMenu2(selection2, selection3, res, sol5);
						}
					}
					break;
				case 2:
					System.out.println(" ");
					System.out.println("- Exercices sur les opérations sur variables -");
					System.out.println(" ");
					System.out.println("Selectionner un exercice :");
					System.out.println(" ");
					System.out.println("10 - Exercice 2.1");
					System.out.println("11 - Exercice 2.2");
					System.out.println("12 - Exercice 2.3");
					System.out.println("13 - Exercice 2.4");
					System.out.println("0 - Retour");
					System.out.println(" ");
					System.out.print("Reponse : ");
					Scanner s3 = new Scanner(System.in);
					selection2 = s3.nextInt();
					if (selection2 != 0) {
						System.out.println("");
						System.out.println(Enonce.get(selection2));
						Resultat res1 = new Resultat2();
						Solution sol5 = new Solution5();
						sousMenu2(selection2, selection3, res1, sol5);
					}
					break;
				case 3:
					System.out.println(" ");
					System.out.println("- Exercices sur les operateurs logiques -");
					System.out.println(" ");
					System.out.println("Selectionner un exercice :");
					System.out.println(" ");
					System.out.println("14 - Exercice 3.1");
					System.out.println("15 - Exercice 3.2");
					System.out.println("16 - Exercice 3.3");
					System.out.println("17 - Exercice 3.4");
					System.out.println("18 - Exercice 3.5");
					System.out.println("19 - Exercice 3.6");
					System.out.println("20 - Exercice 3.7");
					System.out.println("0 - Retour");
					System.out.println(" ");
					System.out.print("Reponse : ");
					Scanner s5 = new Scanner(System.in);
					selection2 = s5.nextInt();
					if (selection2 != 0) {
						System.out.println("");
						System.out.println(Enonce.get(selection2));
						Resultat res3 = new Resultat3();
						Solution sol5 = new Solution5();
						sousMenu2(selection2, selection3, res3, sol5);
					}
					break;
				case 4:
					System.out.println(" ");
					System.out.println("- Exercices sur les boucles -");
					System.out.println(" ");
					System.out.println("Selectionner un exercice :");
					System.out.println(" ");
					System.out.println("21 - Exercice 5.1");
					System.out.println("22 - Exercice 5.2");
					System.out.println("23 - Exercice 5.3");
					System.out.println("24 - Exercice 5.4");
					System.out.println("25 - Exercice 5.5");
					System.out.println("26 - Exercice 5.6");
					System.out.println("27 - Exercice 5.7");
					System.out.println("28 - Exercice 5.7(suite)");
					System.out.println("29 - Exercice 5.8");
					System.out.println("30 - Exercice 5.9");
					System.out.println("31 - Exercice 5.10");
					System.out.println("32 - Exercice 5.11");
					System.out.println("0 - Retour");
					System.out.println(" ");
					System.out.print("Reponse : ");
					Scanner s7 = new Scanner(System.in);
					selection2 = s7.nextInt();
					if (selection2 != 0) {
						System.out.println("");
						System.out.println(Enonce.get(selection2));
						Resultat res4 = new Resultat4();
						Solution sol5 = new Solution5();
						sousMenu2(selection2, selection3, res4, sol5);
					}
					break;
				case 5:
					System.out.println(" ");
					System.out.println("- Exercices sur les tableaux -");
					System.out.println(" ");
					System.out.println("Selectionner un exercice :");
					System.out.println(" ");
					System.out.println("33 - Exercice. Tableau. 5.1");
					System.out.println("34 - Exercice. Tableau. 5.2");
					System.out.println("35 - Exercice. Tableau. 5.3");
					System.out.println("36 - Exercice. Tableau. 5.4");
					System.out.println("37 - Exercice. Tableau. 5.5");
					System.out.println("38 - Exercice. Tableau. 5.6");
					System.out.println("39 - Exercice. Tableau. 5.7");
					System.out.println("0 - Retour");
					System.out.println(" ");
					System.out.print("Reponse : ");
					Scanner s8 = new Scanner(System.in);
					selection2 = s8.nextInt();
					if (selection2 != 0) {
						System.out.println("");
						System.out.println(Enonce.get(selection2));
						Resultat res5 = new Resultat5();
						Solution sol5 = new Solution5();
						sousMenu2(selection2, selection3, res5, sol5);
					}
					break;
				case 6:
					System.out.println(" ");
					System.out.println("- Exercices sur les tableaux à deux dimensions -");
					System.out.println(" ");
					System.out.println("Selectionner un exercice :");
					System.out.println(" ");
					System.out.println("40 - Exercice. Tableau. 6.1");
					System.out.println("41 - Exercice. Tableau. 6.2");
					System.out.println("42 - Exercice. Tableau. 6.3");
					System.out.println("43 - Exercice. Tableau. 6.4");
					System.out.println("44 - Exercice. Tableau. 6.5");
					System.out.println("45 - Exercice. Tableau. 6.6");
					System.out.println("46 - Exercice. Tableau. 6.7");
					System.out.println("47 - Exercice. Tableau. 6.8");
					System.out.println("48 - Exercice. Tableau. 6.9");
					System.out.println("49 - Exercice. Tableau. 6.10");
					System.out.println("50 - Exercice. Tableau. 6.11");
					System.out.println("51 - Exercice. Tableau. 6.12");
					System.out.println("0 - Retour");
					System.out.println(" ");
					System.out.print("Reponse : ");
					Scanner s9 = new Scanner(System.in);
					selection2 = s9.nextInt();
					if (selection2 != 0) {
						System.out.println("");
						System.out.println(Enonce.get(selection2));
						Resultat res6 = new Resultat6();
						Solution sol5 = new Solution5();
						sousMenu2(selection2, selection3, res6, sol5);
					}
					break;
				case 7:
					System.out.println(" ");
					System.out.println("- Exercices sur les fonctions -");
					System.out.println(" ");
					System.out.println("Selectionner un exercice :");
					System.out.println(" ");
					System.out.println("52 - Exercice. Tableau. 7.1");
					System.out.println("53 - Exercice. Tableau. 7.2");
					System.out.println("54 - Exercice. Tableau. 7.3");
					System.out.println("55 - Exercice. Tableau. 7.4");
					System.out.println("56 - Exercice. Tableau. 7.5");
					System.out.println("57 - Exercice. Tableau. 7.6");
					System.out.println("58 - Exercice. Tableau. 7.7");
					System.out.println("59 - Exercice. Tableau. 7.8");
					System.out.println("60 - Exercice. Tableau. 7.9");
					System.out.println("0 - Retour");
					System.out.println(" ");
					System.out.print("Reponse : ");
					Scanner s10 = new Scanner(System.in);
					selection2 = s10.nextInt();
					if (selection2 != 0) {
						System.out.println("");
						System.out.println(Enonce.get(selection2));
						Resultat res7 = new Resultat7();
						Solution sol5 = new Solution5();
						sousMenu2(selection2, selection3, res7, sol5);
					}
					break;
				case 8:
					System.out.println(" ");
					System.out.println("- Autres -");
					System.out.println(" ");
					System.out.println("Selectionner un Jeu :");
					System.out.println(" ");
					System.out.println("61 - Morpion");
					System.out.println("0 - Retour");
					System.out.println(" ");
					System.out.print("Reponse : ");
					Scanner s11 = new Scanner(System.in);
					selection2 = s11.nextInt();
					if (selection2 != 0 && selection2 == 61) {
						System.out.println("");
					System.out.println("Le Jeu Morpion");
						Morpion.lancement();					}
					break;
				default:
					System.out.println("Erreur de saisie : Merci de saisir le bon nombre...");
					lancement(0, selection2, selection3);
					break;
				}
			}
		} catch (InputMismatchException e) {
			System.out.print("Erreur de saisie : Merci de saisir un nombre");
			sousMenu(nombre, selection2, selection2);
		}
	}

	// Cette methode est lanc� dans la methode sousMenu(). Elle permet une fois l'exercice s�l�tionn� de chosir une solution, le resultat ou le retour au menu pr�c�dent.
	public static void sousMenu2(int nombre1, int selection3, Resultat res, Solution5 sol5)throws InputMismatchException {
		try {
		while (selection3 != 0) {
			System.out.println(" ");
			System.out.println("Que souhaitez-vous obtenir ?");
			System.out.println("1 - La solution     2 - Le resultat     0 - Retour");
			System.out.println(" ");
			System.out.print("Reponse : ");
			Scanner s8 = new Scanner(System.in);
			selection3 = s8.nextInt();
			if (selection3 != 0) {
				switch (selection3) {
				case 1:
					System.out.println("");
					System.out.println(" - La solution :"); //Cette partie du menu est fonctionnelle pour les exercices sur les tableaux ou les boucles.
					System.out.println(" ");
					sol5.getSolution(nombre1);
					System.out.println(" ");
					break;
				case 2:
					System.out.println("");
					System.out.println(" - Le resulat :");
					System.out.println(" ");
					res.getSolution(nombre1);
					System.out.println(" ");
					break;
				default:
					sousMenu(30, 30, 30);
					break;
				}
			}
		}
		}catch(InputMismatchException e) {
			sousMenu(30,30,30);
		
		}
	}

}
