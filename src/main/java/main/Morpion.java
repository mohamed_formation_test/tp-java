package main;

import java.util.Arrays;
import java.util.Scanner;

public class Morpion {

	public static void lancement() {
		
		//TO DO : le cas de l'egalit� doit encore �tre trait�...
		
		int i = 1;
		int x = 0;
		int y = 0;
		boolean finPartie = false;
		String joueur = new String();
		String joueur2 = new String();
		String joueur1 = new String();
		char jeton;
		char jeton1 = 'X';
		char jeton2 = 'O';
		int dimension = creationSupport();
		int compteurl = 0;
		int compteurc = 0;
		int compteurd = 0;
		int compteurd2 = 0;
		int centre = 0;
		char[][] tab = new char[dimension][dimension];
		initaliseTab(tab);
		while (i < 3) {
			if (i == 1) {
				joueur1 = creationJoueur(i);
			} else {
				joueur2 = creationJoueur(i);
			}
			i++;
		}
		centre = tab.length / 2;
		joueur = joueur1;
		jeton = jeton1;
		while (finPartie == false) {
			x = saisirLigne(joueur);
			if (x > tab.length - 1 || x < 0) {
				System.out.println("Erreur de saisie");
				x = saisirLigne(joueur);
			}
			y = saisirColonne(joueur);
			if (y > tab.length - 1 || y < 0) {
				System.out.println("Erreur de saisie");
				y = saisirColonne(joueur);
			}
			if (tab[x][y] == ' ') {
				tab[x][y] = jeton;
			} else {

			}
			finPartie = jouer(centre, tab, compteurl, compteurc, compteurd, compteurd2, finPartie);
			joueur = changementJoueur(joueur, joueur1, joueur2, finPartie);
			jeton = changementJeton(joueur, joueur1, joueur2, jeton, jeton1, jeton2);
			affichageTab(tab);
		}
		afficheVainqueur(joueur);
	}

	private static int saisirLigne(String joueur) {
		System.out.println(joueur + ": Merci de saisir la ligne � remplir : ");
		System.out.print("Reponse : ");
		Scanner s1 = new Scanner(System.in);
		return s1.nextInt();
	}

	private static String changementJoueur(String joueur, String joueur1, String joueur2, boolean finPartie) {
		if (joueur == joueur1 && finPartie == false) {
			joueur = joueur2;

		} else if (joueur == joueur2 && finPartie == false) {
			joueur = joueur1;

		}
		return joueur;
	}

	private static char changementJeton(String joueur, String joueur1, String joueur2, char jeton, char jeton1,
			char jeton2) {
		if (joueur == joueur1) {
			jeton = jeton1;
		} else {
			jeton = jeton2;
		}
		return jeton;
	}

	private static boolean jouer(int centre, char[][] tab, int compteurl, int compteurc, int compteurd, int compteurd2,
			boolean finPartie) {
		compteurd = 0;
		compteurd2 = 1;
		for (int k = 0; k < tab.length - 1; k++) {
			compteurl = 0;
			compteurc = 0;
			for (int j = 0; j < tab.length - 1; j++) {
				if (tab[j][k] != ' ') {
					if (tab[j][k] == tab[j + 1][k]) {
						compteurc++;
					}
				}
				if (tab[k][j] != ' ') {
					if (tab[k][j] == tab[k][j + 1]) {
						compteurl++;
					}
				}
				if (k == j && tab[k][j] != ' ') {
					if (tab[k][j] == tab[k + 1][j + 1]) {
						compteurd++;
					}
				}
				if (k == centre && j == centre && tab[centre][centre] != ' ') {
					if (tab[centre][centre] == tab[centre - 1][centre + 1]
							&& tab[centre][centre] == tab[centre + 1][centre - 1]) {
						compteurd2++;
					}
				}
			}
			if (compteurl == 2 || compteurc == 2 || compteurd == 2 | compteurd2 == 2) {

				finPartie = true;
			}
		}
		return finPartie;
	}

	private static void afficheVainqueur(String joueur) {
		System.out.println(" ");
		System.out.println("****************************");
		System.out.println(" ");
		System.out.println(joueur + " " + "a gagn� la partie");
		System.out.println("  ");
		System.out.println("*****************************");
	}

	private static int saisirColonne(String joueur) {
		System.out.println(joueur + " : Merci de saisir la colonne � remplir : ");
		System.out.print("R�ponse : ");
		Scanner s2 = new Scanner(System.in);
		return s2.nextInt();
	}

	private static void affichageTab(char[][] tab) {
		for (int i = 0; i <= tab.length - 1; i++) {
			for (int j = 0; j <= tab.length - 1; j++) {
				System.out.print("[" + tab[i][j] + "]");
			}
			System.out.println("");
		}
	}

	private static void initaliseTab(char[][] tab) {
		for (int i = 0; i <= tab.length - 1; i++) {
			for (int j = 0; j <= tab.length - 1; j++) {
				tab[i][j] = ' ';
				System.out.print("[" + tab[i][j] + "]");
			}
			System.out.println("");
		}
	}

	private static String creationJoueur(int i) {
		System.out.println("Merci de saisir le nom du joueur" + " " + i + " " + ":");
		System.out.print("Reponse : ");
		Scanner s2 = new Scanner(System.in);
		return s2.next();
	}

	private static int creationSupport() {
		System.out.println("Merci de saisir la dimension de votre morpion (3)");
		System.out.print("Reponse : ");
		Scanner s = new Scanner(System.in);
		return s.nextInt();
	}

}
