package main;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

public enum Enonce {

    exo11(1, "Exercice 1.1. \n Quelles seront les valeurs des variables A et B après exécution des instructions suivantes ? \n Variables A, B en entier \n Début \n A <- 1 \n B <- A + 3 \n A <- 3 \n Fin"),
    exo12(2, "Exercice 1.2. \n Quelles seront les valeurs des variables A, B et C après exécution des instructions suivantes ? \n Variables A, B et C en entier \n Début \n A <- 5 \n B <- 3 \n C <- A + B \n A <- 2 \n C <- B - A \n Fin"),
    exo13(3, "Exercice 1.3. \n Quelles seront les valeurs des variables A et B après exécution des instructions suivantes ? \n Variables A, B en entier \n Début \n A <- 5 \n B <- A + 4 \n A <- A + 1 \n B <- A - B \n Fin"),
    exo14(4, "Exercice 1.4. \n Quelles seront les valeurs des variables A, B et C après exécution des instructions suivantes ? \n Variables A, B et C en entier \n Début \n A <- 3 \n B <- 10 \n C <- A + B \n A <- C \n Fin"),
    exo15(5, "Exercice 1.5. \n Quelles seront les valeurs des variables A et B après exécution des instructions suivantes ? \n Variables A, B en entier \n Début \n A <- 5 \n B <- 2 \n A <- B \n B <- A \n Fin"),
    exo16(6, "Exercice 1.6. \n Plus difficile, mais c’est un classique absolu, qu’il faut absolument maîtriser : \n écrire un algorithme permettant d’échanger les valeurs de deux variables A et B, \n et ce quel que soit leur contenu préalable."),
    exo17(7, "Exercice 1.7. \n Une variante du précédent : on dispose de trois variables A, B et C. \n Écrivez un algorithme transférant à B la valeur de A, à C la valeur de B et à A la valeur de C\n (toujours quels que soient les contenus préalables de ces variables)."),
    exo18(8, "Exercice 1.8. \n Que produit l’algorithme suivant ? \n Variables A, B et C en Caractères \n Début \n A <- \"423\" \n B <- \"12\" \n C <- A + B \n Fin"),
    exo19(9, "Exercice 1.9. \n Que produit l’algorithme suivant ? \n Variables A, B et C en Caractères \n Début \n A <- \"423\" \n B <- \"12\" \n C <- A & B \n Fin"),
    exo21(10, "Exercice 2.1. \n Quel résultat produit le programme suivant ? \n Variables val, double numériques \n Début \n val <- 231 \n double <- val * 2 \n Ecrire val \n Ecrire double \n Fin"),
    exo22(11, "Exercice 2.2. \n Écrire un programme qui demande un nombre à l’utilisateur, puis qui calcule et affiche le carré de ce nombre."),
    exo23(12, "Exercice 2.3 \n Ecrire un programme qui demande son prénom à l'utilisateur,\n et qui lui réponde par un charmant « Bonjour » suivi du prénom. On aura ainsi le dialogue suivant : ? \n machine : Quel est votre prénom ? \n utilisateur : Marie-Cunégonde. \n machine : Bonjour, Marie Cunégonde ! "),
    exo24(13, "Exercice 2.4. \n Écrire un programme qui lit le prix HT d’un article, le nombre d’articles et \n le taux de TVA, et qui fournit le prix total TTC correspondant. \n Faire en sorte que des libellés apparaissent clairement."),
    exo31(14, "Exercice 3.1. \n Ecrire un algorithme qui demande un nombre à l’utilisateur, et\n l’informe ensuite si ce nombre est positif ou négatif (on laisse de côté le cas où le nombre vaut zéro)."),
    exo32(15, "Exercice 3.2. \n Ecrire un algorithme qui demande deux nombres à l’utilisateur et\n l’informe ensuite si leur produit est négatif ou positif (on laisse de côté le cas où le produit est nul).\n Attention toutefois : on ne doit pas calculer le produit des deux nombres. "),
    exo33(16, "Exercice 3.3. \n Ecrire un algorithme qui demande trois noms à l’utilisateur et\n l’informe ensuite s’ils sont rangés ou non dans l’ordre alphabétique. "),
    exo34(17, "Exercice 3.4. \n Ecrire un algorithme qui demande un nombre à l’utilisateur, et\n l’informe ensuite si ce nombre est positif ou négatif (on inclut cette fois le traitement du cas où le nombre vaut zéro"),
    exo35(18, "Exercice 3.5. \n Ecrire un algorithme qui demande l’âge d’un enfant à l’utilisateur.\n Ensuite, il l’informe de sa catégorie : \n - Poussin de 6 à 7 ans \n - Pupille de 8 à 9 ans \n - Minime de 10 à 11 ans \n - Cadet après 12 ans"),
    exo36(19, "Exercice 3.6. \n Ecrire un algorithme qui demande un nombre entier à l'utilisateur, puisqui teste et affiche s'il est divisible par 3 "),
    exo37(20, "Exercice 3.7. \n Le prix des photocopies dans une reprographie varie selon le nombre demandé: \n 15 cents la copie pour un nombre de copies inférieur à 10, \n 10 cents pour un nombre compris entre 10 et 20 et \n 5 cents au-delà.Ecrire un algorithme qui demande à l’utilisateur le nombre de photocopies effectuées, \n qui calcule et affiche le prix à payer"),
    exo51(21, "Exercice 5.1. \n Ecrire un algorithme qui demande à l’utilisateur un nombre compris\n entre 1 et 3 jusqu’à ce que la réponse convienne."),
    exo52(22, "Exercice 5.2. \n Ecrire un algorithme qui demande un nombre compris entre 10 et 20, jusqu’à ce que la réponse convienne.\n En cas de réponse supérieure à 20, on fera apparaître un message :\n « Plus petit ! », et inversement, « Plus grand ! » si le nombre est inférieur à 10."),
    exo53(23, "Exercice 5.3. \n Ecrire un algorithme qui demande un nombre de départ, et qui ensuite affiche les dix nombres suivants. \n Par exemple, si l'utilisateur entre le nombre 17, le programme affichera les nombres de 18 à 27."),
    exo54(24, "Exercice 5.4. \n Réécrire l'algorithme précédent, en utilisant cette fois l'instruction `Pour`. "),
    exo55(25, "Exercice 5.5. \n Ecrire un algorithme qui demande un nombre de départ, \n et qui ensuite écrit la table de multiplication de ce nombre,\n présentée comme suit (cas où l'utilisateur entre le nombre 7) : \n Table de 7 : \n 7 x 1 = 7 \n 7 x 2 = 14 \n 7 x 3 = 21 \n ... \n 7 x 10 = 70"),
    exo56(26, "Exercice 5.6. \n Ecrire un algorithme qui demande un nombre de départ, \n et qui calcule la somme des entiers jusqu’à ce nombre.\n Par exemple, si l’on entre 5, le programme doit calculer :1 + 2 + 3 + 4 + 5 = 15. \n NB : on souhaite afficher uniquement le résultat, pas la décomposition du calcul."),
    exo57(27, "Exercice 5.7. \n Ecrire un algorithme qui demande successivement 20 nombres à l’utilisateur,\n et qui lui dise ensuite quel était le plus grand parmi ces 20 nombres : \n Ex : Entrez le nombre numero 1 : 12 \n Entrez le nombre numero 2 : 14 \n etc. \n Entrez le nombre numero 20 : 6 \n Le plus grand de ces nombre est : 14"),
    exo572(28, "Exercice 5.7.2. \n (Suite)Modifiez ensuite l’algorithme pour que le programme affiche de surcroît en quelle position avait été saisie ce nombre : \n Ex : C'etait le nombre numéro 2 "),
    exo58(29, "Exercice 5.8. \n Ecrire un algorithme qui demande un nombre de départ, et qui calcule sa factorielle. \n NB : la factorielle de 8, notée 8 !, vaut1 x 2 x 3 x 4 x 5 x 6 x 7 x 8 "),
    exo59(30, "Exercice 5.9. \n Réécrire l’algorithme précédent, mais cette fois-ci \n on ne connaît pas d’avance combien l’utilisateur souhaite saisir de nombres. \n La saisie des nombres s’arrête lorsque l’utilisateur entre un zéro."),
    exo60(31, "Exercice 5.10. \n Lire la suite des prix (en euros entiers et terminée par zéro) des achats d’un client. \n Calculer la somme qu’il doit, lire la somme qu’il paye, et \n simuler la remise de la monnaie en affichant les textes \n \"10 Euros\", \"5 Euros\" et \"1 Euro\" autant de fois qu’il y a de coupures de chaque sorte à rendre. "),
    exo61(32, "Exercice 5.11. \n Ecrire un algorithme qui détermine le premier nombre \n entier N tel que la somme de 1 à N dépasse strictement 100 "),
	exo62(33, "Exercice. Tableau. 5.1. \n  Ecrire un algorithme qui permet de trouver \n le nombre d’occurrences d’une valeur entière dans un tableau de taille N."),
	exo63(34, "Exercice. Tableau. 5.2. \n Comment déterminer si un tableau d’entiers\n à une dimension est trié par ordre croissant ? Ecrire l'algorithme."),
	exo64(35, "Exercice.Tableau. 5.3. \n Ecrire un algorithme qui calcule le plus grand écart \n dans un tableau (l’écart est la valeur absolue de la différence de deux éléments)."),
	exo65(36, "Exercice.Tableau. 5.4. \n Ecrire l’algorithme effectuant le décalage des  éléments d’un tableau."),
	exo66(37, "Exercice.Tableau. 5.5. \n Ecrire un algorithme qui inverse l’ordre des éléments d’un tableau de taille N."),
	exo67(38, "Exercice.Tableau. 5.6. \n Ecrire l'algorithme du tri à sélection en supposant \n qu'il est appliqué sur un tableau d'entiers déjà rempli de taille N donnée."),
	exo68(39, "Exercice.Tableau. 5.7. \n Ecrire l'algorithme du tri à bulles en supposant \n qu'il est appliqué sur un tableau d'entiers déjà rempli de taille N donnée."),
	exo69(40, "Ecrire un algorithme permettant de saisir les données d’un tableau à deux dimensions (10,4), \n de faire leur somme, produit et moyenne et de les afficher \n avec les résultats de calcul à l’écran."),
	exo70(41, "Ecrire un algorithme qui calcule la somme des éléments de la diagonale d’une matrice carrée M(n,n) donnée"),
	exo71(42, "Ecrire un algorithme permettant de construire dans une matrice carrée P et d’afficher le triangle de PASCAL de degré N"),
	exo72(43, "Soit un tableau T à deux dimensions (12, 8) préalablement rempli de valeurs numériques.\n Écrire un algorithme qui recherche la plus grande valeur au sein de ce tableau."),
	exo73(44, "On suppose qu’il y a 4 modèles et 4 vendeurs. Écrire un algorithme qui crée le tableau des ventes et\n lit au clavier les données permettant de le remplir."),
	exo74(45, "Écrire un algorithme qui donne le nombre d’exemplaires vendus pour chacun des modèles."),
	exo75(46, "On donne le prix de chaque modèle dans un second tableau à une seule dimension.\n Écrire l’algorithme qui calcule le chiffre d’affaire généré par chacun des vendeurs,\n c’est à dire le total de ses ventes exprimé en euros."),
	exo76(47, "Écrire un algorithme qui lit une matrice carrée d’ordre 5, puis teste si cette matrice est antisymétrique."),
	exo77(48, "Écrire un algorithme qui teste si le tableau t est régulier, c’est-à-dire si toutes ses lignes ont la même taille"),
	exo78(49, "Écrire un algorithme qui s’assure que les tableaux t1 et t2 sont réguliers et de mêmes dimensions et\n fournit dans ce cas leur somme en résultat"),
	exo79(50, "Écrire un algorithme qui remplie un tableau de deux dimension et de même taille (5,5) est qui calcule\n la somme de la diagonale principale de ce dernier"),
	exo80(51, "Écrire un algorithme qui vérifie si un carré est magique."),
	exo81(52, "Soit le programme suivant : 1.Identifier les paramètres réels et les paramètres formels.\n2. Qu'affiche ce programme en supposant que l'utilisateur saisisse 2 dans x et 3 dans y ?\n Modifier le code pour obtenir un résultat plus logique."),
	exo82(53,  "Ecrire une procédure échanger qui échange la valeur de deux variables.\nOn donne ci­ dessous un exemple d'appel de la procédure échange"),
	exo83(54, "Que fait cette procédure mystere.2. Au lieu d'une procédure,\n peut­ on utiliser une fonction ? Si oui, en donner l'algorithme"),
	exo84(55, "Ecrire une fonction surface qui calcule la surface d'un rectangle de largeur l et longueur L \n(ces deux grandeurs étant passées en paramètre).Utiliser cette fonction surface dans un programme \n principal qui demande à l'utilisateur les dimensions d'une pièce rectangulaire\n et qui indique le nombre de pots de shampoing cirant pour tomettes nécessaires pour\n cirer le sol de cette pièce. Un pot permet de traiter 10 m2"),
	exo85(56, "Ecrire une fonction qui prend en paramètre un temps exprimé en heures, minutes etsecondes, et qui renvoie ce temps en secondes."),
	exo86(57, "Quelle action réalise la fonction récursive somtab ci­dessous ? \nPour aider à répondre à cette question, \n on donne un exemple d'appel de cette fonction somtab."),
	exo87(58, "Ecrire une fonction récursive qui calcule la somme S des N premiers carrés.\nPour exemple, si N vaut 3, alors S = 3²+ 2²+ 1²= 14"),
	exo88(59, "Ecrire une fonction récursive qui calcule le produit de deux nombres a et b.\nContrainte : on impose de ne pas utiliser l'opérateur de multiplication « × »."),
	exo89(60, "Ecrire une fonction récursive qui calcule l’occurrence (le nombre d'apparitions)\n d'un entier val dans un tableau T à une dimension de taille N.");
	
	
	
	
	
	
    private static final Map<Integer, String> searchEnonce = new HashMap<Integer, String>();

    static {
        for (Enonce e : EnumSet.allOf(Enonce.class))
            searchEnonce.put(e.getNumero(), e.texte);

    }

    private int numero;
    private String texte;

    private Enonce(int numero, String texte) {
        this.numero = numero;
        this.texte = texte;
    }

    public int getNumero() {
        return numero;
    }


    public static String get(int numero) {
        return searchEnonce.get(numero);
    }





}
