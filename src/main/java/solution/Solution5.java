package solution;

import java.util.Arrays;
import java.util.Scanner;

public class Solution5 extends Solution{
	public void getSolution(int numero) {

		switch (numero) {
		case 33:
			detailExo51Tableau();
			break;
		case 34:
			detailExo52Tableau();
			break;
		case 35:
			detailExo53Tableau();
			break;
		case 36:
			detailExo54Tableau();
			break;
		case 37:
			detailExo55Tableau();
			break;
		case 38:
			detailExo56Tableau();
			break;
		case 39:
			detailExo57Tableau();
			break;

		}

	}

	private void detailExo51Tableau() {

		System.out.println("int nombre = 0;");
		System.out.println("int occurence = 0;");
		System.out.println("System.out.println(\"Merci de saisir le nombre à vérifier?\");");
		System.out.println("Scanner s = new Scanner(System.in);");
		System.out.println("nombre = s.nextInt();");
		System.out.println("\tfor(int i = 0 ; i<= tab.length-1; i++) {");
		System.out.println("\t\tif(nombre == tab[i]) {");
		System.out.println("\t \t occurence ++;");
		System.out.println("\t\t}");
		System.out.println("\t}");
		System.out.println(
				"System.out.println(\"Le nombre\" + \" \" + nombre + \" \" + \"est présent\" +\" \"+ occurence + \" \" + \"fois.\");");
	}

	private void detailExo52Tableau() {
		System.out.println("\t \t int[] tableau = {3,45,6,9,78};");
		System.out.println("\t \t boolean trie = true;");
			System.out.println("\t \t for (int i = 0; i <= tableau.length - 2; i++) {");
			System.out.println("\t \t \t if (tableau[i] > tableau[i + 1]) {");
				System.out.println("\t\t\t\t trie = false;");
				System.out.println("\t\t\t\t i = tableau.length - 1;");
				System.out.println("\t \t \t}");
				System.out.println("\t \t}");
				System.out.println("\t \t if (trie == true) {");
				System.out.println(
						"\t \t \t System.out.println(\"Le tableau trié de maniere croissante.\");");
		System.out.println("\t\t} else {");
		System.out.println(
				"\t \t \t System.out.println(\"Le tableau n'est pas trié de maniere croissante.\");");
		System.out.println("\t\t}");

		}
	

	private void detailExo53Tableau() {
		System.out.println("\t \t int[] tab = {3,45,6,9,78};");	
		System.out.println("\t \t int nombre = 0;");
		System.out.println("\t \t int max = 0;");
		System.out.println("\t \t int min = 0;");
		System.out.println("\t \t int ecart = 0;");
				System.out.println("\t \t for (int i = 0; i <= tab.length - 1; i++) {);");
						System.out.println("\t \t \t if (i == 0) {");
						System.out.println("\t \t \t \t min = tab[0];");
						System.out.println("\t \t \t \t max = tab[0];");
						System.out.println("\t \t \t} else if (tab[i] > max) {");
						System.out.println("\t \t \t \t max = tab[i];");

						System.out.println("\t \t \t} else if (tab[i] < min) {");

						System.out.println("\t \t \t \t	min = tab[i];");
						System.out.println("\t \t\t}");
						System.out.println("\t \t }");

						System.out.println("\t \t ecart = max - min;");
						System.out.println("\t \t System.out.println(\"\");");
						System.out.println("\t \t System.out.println(\"Le plus grand ecart entre deux nombre dans ce tableau est de\" + \" \" + ecart + \".\");");
	}

	private void detailExo54Tableau() {
		System.out.println("\t \t char[] tab = {'D','E','C','A','L','A','G','E'};");	
		System.out.println("\t \t char temp = ' ';");
		System.out.println("\t \t \t for (int i = 0; i < tabDecalage.length - 1; i++) {");
		System.out.println("\t \t \t \t	temp = tabDecalage[i];");
		System.out.println("\t \t \t \t	tabDecalage[i] = tabDecalage[i + 1];");
		System.out.println("\t \t \t \t	tabDecalage[i + 1] = temp;");
		System.out.println("\t \t \t}");
	}

	private void detailExo55Tableau() {
		System.out.println("\t \t int[] tab = {3,45,6,9,78};");
		System.out.println("\t \t int i = 0;");
		System.out.println("\t \t int j = tab.length - 1;");
		System.out.println("\t \t int temp = 0;");
		System.out.println("\t \t \t while (i < j) {");
		System.out.println("\t \t \t \t temp = tab[i];");
				System.out.println("\t \t \t \t tab[i] = tab[j];");
				System.out.println("\t \t \t \t tab[j] = temp;");
				System.out.println("\t \t \t \t i++;");
				System.out.println("\t \t \t \t j--;");
				System.out.println("\t \t \t}");
	}

	private void detailExo56Tableau() {
		System.out.println("\t \t int[] tab = {3,45,6,9,78};");
		System.out.println("\t \t int min = 0;");
		System.out.println("\t \t int indiceMin = 0;");
		System.out.println("\t \t int indice = 0;");
		System.out.println("\t \t int temp = 0;");

		System.out.println("\t \t  while (indice < tab.length) {");

				System.out.println("\t \t \t min = tab[indice];");

				System.out.println("\t \t \t \t  for (int i = indice; i < tab.length; i++) {");
				System.out.println("\t \t \t \t \t if (tab[i] < min) {");
				System.out.println("\t \t \t \t \t \t min = tab[i];");
				System.out.println("\t \t \t \t \t \t indiceMin = i;");

				System.out.println("\t \t \t \t \t}");
				System.out.println("\t \t \t \t}");
				System.out.println("\t \t \t temp = tab[indice];");
				System.out.println("\t \t \t tab[indice] = tab[indiceMin];");
				System.out.println("\t \t \t tab[indiceMin] = temp;");
				System.out.println("\t \t \t afficheTab(tab);");
				System.out.println("\t \t \t indice++;");
				System.out.println("\t \t}");
	}

	private void detailExo57Tableau() {
		System.out.println("\t \tint[] tab = {3,45,6,9,78};");
		System.out.println("\t \tint temp = 0;");
		System.out.println("\t \tint i = 0;");
		System.out.println("\t \twhile (i <= tab.length-1) {");
		System.out.println("\t \t \tfor (int j = 0; j < (tab.length-1)-i; j++) {");
		System.out.println("\t \t \t \tif (tab[j] > tab[j + 1]) {");
		System.out.println("\t \t \t \t \ttemp = tab[j];");
		System.out.println("\t \t \t \t \ttab[j] = tab[j + 1];");
				System.out.println("\t \t \t \t \ttab[j + 1] = temp;");
				System.out.println("\t \t \t \t}");
				System.out.println("\t \t \t}");
				System.out.println("\t \t i++;");
				System.out.println("\t \t}");
		
	}

}
