# Exercices Variable


## Exercice 1

> Écrire un programme qui demande un nombre à l’utilisateur, puis qui calcule et affiche le carré de ce nombre.

<br/>
<br/>

## Exercice 2

> Ecrire un programme qui demande son prénom à l'utilisateur, et qui lui réponde par un charmant « Bonjour » suivi du prénom. 

> On aura ainsi le dialogue suivant : 
* machine : Quel est votre prénom ? utilisateur : Marie-Cunégonde. 
* machine : Bonjour, Marie Cunégonde ! 

<br/>
<br/>

## Exercice 3

> Écrire un programme qui lit le prix HT d’un article, le nombre d’articles et le taux de TVA, et qui fournit le prix total TTC correspondant. Faire en sorte que des libellés apparaissent clairement.

<br/>
<br/>

