# Exercices Structures itératives et conditionnelles (suite)

<br/>
<br/>

## Exercice 1

> Ecrire un algorithme qui demande un nombre de départ, \n et qui calcule la somme des entiers jusqu’à ce nombre.
> Par exemple, si l’on entre 5, le programme doit calculer :1 + 2 + 3 + 4 + 5 = 15. 
> NB : on souhaite afficher uniquement le résultat, pas la décomposition du calcul.

<br/>
<br/>

## Exercice 2

> Ecrire un algorithme qui demande successivement 20 nombres à l’utilisateur,et qui lui dise ensuite quel était le plus grand parmi ces 20 nombres :  <br/>
> Ex : <br/>
> Entrez le nombre numero 1 : 12. <br/>
> Entrez le nombre numero 2 : 14. <br/>
> etc. 
> Entrez le nombre numero 20 : 6. <br/>
> <br/>
> Le plus grand de ces nombre est : 14.

<br/>
<br/>

## Exercice 3

> Modifiez ensuite l’algorithme pour que le programme affiche de surcroît en quelle position avait été saisie ce nombre :<br/>
> Ex : C'etait le nombre numéro 2.

<br/>
<br/>

## Exercice 4

> Ecrire un algorithme qui demande un nombre de départ, et qui calcule sa factorielle.<br/>
> NB : la factorielle de 8, notée 8 !, vaut1 x 2 x 3 x 4 x 5 x 6 x 7 x 8.

<br/>
<br/>

## Exercice 5 

> Réécrire l’algorithme précédent, mais cette fois-ci on ne connaît pas d’avance combien l’utilisateur souhaite saisir de nombres. <br/>
> La saisie des nombres s’arrête lorsque l’utilisateur entre un zéro.

<br/>
<br/>

## Exercice 4

> Lire la suite des prix (en euros entiers et terminée par zéro) des achats d’un client. 
> Calculer la somme qu’il doit, lire la somme qu’il paye, et simuler la remise de la monnaie en affichant les textes "10 Euros", "5 Euros" et "1 Euro" autant de fois qu’il y a de coupures de chaque sorte à rendre.

<br/>
<br/>

## Exercice 5

> Ecrire un algorithme qui détermine le premier nombre entier N tel que la somme de 1 à N dépasse strictement 100.